{
	nixpkgs ? import <nixpkgs> {},
	cache ? null,
	naersk ? nixpkgs.pkgs.callPackage (fetchTarball "https://github.com/nmattia/naersk/archive/master.tar.gz") {},
	smb1 ? null,
}:
with nixpkgs; let
	args = {
		root = pkgs.nix-gitignore.gitignoreSource [
			"*"
			"!Cargo*"
			"!src/"
		] ./.;
		buildInputs = [ pkgs.sqlite ];
		doCheck = true;
	};
in rec {
	mario-solver = naersk.buildPackage args;

	mario-solver-instrumented = if smb1 == null then null else
		naersk.buildPackage (args // {
			name = "mario-solver-instrumented";
			RUSTFLAGS = "-Cprofile-generate=/tmp/llvm-profile";
			doCheck = false;
		});

	profile-data = if mario-solver-instrumented == null then null else
		pkgs.runCommand "mario-solver.profdata" {} ''
			${mario-solver-instrumented}/bin/mario-solver -j$NIX_BUILD_CORES \
				--core=${pkgs.libretro.fceumm}/lib/retroarch/cores/fceumm_libretro.so \
				--rom=${smb1} \
				--debug --fast-start --entrance-mode=1 --entrance=3
			${pkgs.rustc.llvm}/bin/llvm-profdata merge -o $out /tmp/llvm-profile
		'';

	mario-solver-pgo = if profile-data == null then null else
		naersk.buildPackage (args // {
			name = "mario-solver-pgo";
			RUSTFLAGS = "-Cprofile-use=${profile-data}";
			doCheck = false;
		});
}
