# Mario Solver

Searches for the optimal solution to a Super Mario Bros. level.

## Getting Started

You need:

- A libretro NES core. I have tested FCEUmm and Nestopia.
- A copy of SMB1.

```sh
cargo run --core=/usr/lib/libretro/fceumm_libretro.so --rom=smb1.nes --out=./mario-solver
```

## Current Limitations

The prefix is how hard it would be to overcome.

- **Moderate**: Only players the first level.
- **Easy**: Only uses human allowed input. (No Up+Down or Left+Right).
- **Moderate**: We ignore some RAM values when comparing states. This means that we won't necessarily find the optimal solution, but makes the solver run **much** faster.
- We assume that solving the level involves getting to a given X coordinate.
	- **Moderate**: Allow a known-pipe exit.
	- **Hard**: Allow any exit.
- **Moderate**: We have a hard coded max-speed assumption. Changing it would be fairly easy but it would make solving untenable.
- **Moderate**: Runs on a single machine. The cores are already spawned in a separate process and communicate over a socket so running across the network would be easy. However this would make some current performance problems much more costly.

