use std::io::{Read,Write};

/// RAM addresses that don't affect game state..
pub const WORK_RAM: &[u16] = &[
	0x000, // Scratch Space.
	0x005, //
	// 0x009, // Frame counter.
	// 0x00A, // A and B buttons.
	// 0x00B, // Up and Down buttons.
	// 0x00D, // A and B buttons.
	0x0F5,
	0x0F7,
	0x0F8,
	0x0F9,
	0x300,
	0x301,
	0x305,
	0x6D4, // Coin and Block Animation Timer
	0x6E0,
	0x6E5,
	0x6E6,
	0x6E7,
	0x6E8,
	0x6E9,
	0x6EA,
	0x6EB,
	0x6EC,
	0x6ED,
	0x6EF,
	0x6F0,
	0x6F1,
	0x6F2,
	0x6F3,
	0x6F4,
	0x6F5,
	0x6F6,
	0x6F7,
	0x6F8,
	0x6F9,
	0x6FA,
	0x6FC, // Player 1 Input
	0x6FD, // Player 2 Input
	0x6FB,
	0x74A, // Previous frame's controller for pause checking.
	0x77F, // Framerule Timer
	0x787, // GameTimer Control Timer
	0x7A0,
	0x7A7,
	0x7A8,
	0x7A9,
	0x7AA,
	0x7AB,
	0x7AC,
	0x7AD,
	0x7B0,
	0x7B3,
	0x7B4,
	0x7B6,
	0x7B8,
	0x7B9,
	0x7BA,
	0x7C1,
	0x7C7,
	0x7FA,
];

pub(crate) struct Emulator {
	child: std::os::unix::net::UnixStream,
	child_id: nix::unistd::Pid,
	buf: Vec<u8>,
}

impl Emulator {
	pub fn new(
		core: &std::path::Path,
		rom: &std::path::Path,
		scratch: &std::path::Path)
	-> Result<Self, failure::Error>
	{
		let (child, parent) = std::os::unix::net::UnixStream::pair()?;

		let child_id = match unsafe { nix::unistd::fork() }? {
			nix::unistd::ForkResult::Parent { child, .. } => child,
			nix::unistd::ForkResult::Child => EmulatorChild {
				parent,
				emulator: retro_rs::Emulator::create(core, rom),
				buf: Vec::new(),
				save_path: scratch.join("saves/000000000000"),

				save_buf: Vec::new(),
			}.run(),
		};

		Ok(Emulator {
			child,
			child_id,
			buf: Vec::new(),
		})
	}

	/// Flush local commands and wait until they are confirmed processed.
	pub fn flush_full(&mut self) -> Result<(), failure::Error> {
		self.send(Request::Flush)?;
		if !self.read::<bool>()? {
			return Err(failure::format_err!("Expected true"));
		}
		Ok(())
	}

	fn flush(&mut self) -> Result<(), failure::Error> {
		self.child.write_all(&self.buf)?;
		self.buf.clear();
		Ok(())
	}

	fn write(&mut self, v: impl serde::Serialize + std::fmt::Debug) -> Result<(), failure::Error> {
		let base = self.buf.len();
		self.buf.extend(&0u32.to_le_bytes());
		let data = self.buf.len() as u32;
		bincode::serialize_into(&mut self.buf, &v)?;
		let end = self.buf.len() as u32;

		debug_assert!(end != data, "Zero sized message.");
		self.buf[base..][..4].copy_from_slice(&(end - data).to_le_bytes());

		if self.buf.len() > 1024 * 1024 {
			self.flush()?;
		}

		Ok(())
	}

	fn send(&mut self, v: impl serde::Serialize + std::fmt::Debug) -> Result<(), failure::Error> {
		self.write(v)?;
		self.flush()
	}

	fn read<T: serde::de::DeserializeOwned>(&mut self)
	-> Result<T, failure::Error>
	{
		debug_assert_eq!(self.buf.len(), 0);

		let mut buf = [0; 4];
		self.child.read_exact(&mut buf[..])?;
		let size = u32::from_le_bytes(buf);

		if size == 0 {
			let e: String = self.read()?;
			return Err(failure::format_err!("{}", e))
		}

		self.buf.resize_with(size as usize, Default::default);
		self.child.read_exact(&mut self.buf)?;

		let r = bincode::deserialize(&self.buf)?;

		self.buf.clear();

		Ok(r)
	}

	pub fn snapshot(&mut self) -> Result<(), failure::Error> {
		self.write(&Request::Snapshot)?;
		Ok(())
	}

	pub fn save(&mut self, slot: u32) -> Result<(), failure::Error> {
		self.send(Request::Save(slot))?;
		Ok(())
	}

	pub fn load(&mut self, base: &crate::SaveState) -> Result<(), failure::Error> {
		self.write(&Request::Load(base.id))?;
		Ok(())
	}

	pub fn load_path(&mut self, save: std::path::PathBuf) -> Result<(), failure::Error> {
		self.write(&Request::LoadPath(save))?;
		Ok(())
	}

	pub fn restore(&mut self) -> Result<(), failure::Error> {
		self.write(&Request::Restore)?;
		Ok(())
	}

	pub fn run(&mut self, script: &crate::Script)
	-> Result<RunResult, failure::Error>
	{
		self.load(&script.base)?;

		for input in &script.inputs {
			self.run_input(*input)?;
		}

		self.get_state()
	}

	pub fn run_one(&mut self, input: crate::Input) -> Result<RunResult, failure::Error> {
		self.run_input(input)?;
		self.get_state()
	}

	fn run_input(&mut self, input: crate::Input) -> Result<(), failure::Error> {
		self.write(Request::Frame(input))
	}

	fn get_state(&mut self) -> Result<RunResult, failure::Error> {
		self.send(Request::GetState)?;
		self.read()
	}

	pub fn get_ram(&mut self) -> Result<Vec<u8>, failure::Error> {
		self.send(Request::GetRam)?;
		self.read()
	}

	pub fn render(&mut self, path: std::path::PathBuf) {
		self.send(Request::Render(path)).unwrap();
	}

	pub fn run_right(&mut self) -> Result<(), failure::Error> {
		for _ in 0..40 {
			self.run_input(crate::Input::default().right().b(true))?;
		}

		Ok(())
	}

	pub fn load_level(&mut self, level: Level) -> Result<(), failure::Error> {
		self.send(&Request::Reset)?;
		self.send(&Request::LoadLevel(level))
	}
}

impl std::ops::Drop for Emulator {
	fn drop(&mut self) {
		if let Err(e) = self.flush_full() {
			eprintln!("Error flushing output: {:?}", e);
		}
		if let Err(e) = nix::sys::signal::kill(self.child_id, nix::sys::signal::Signal::SIGINT) {
			eprintln!("Error killing child: {:?}", e);
		}
	}
}

#[derive(Copy,Clone,Debug,serde_derive::Deserialize,serde_derive::Serialize)]
pub(crate) struct Level {
	pub world: u8,
	pub level: u8,
	pub area: u8,
	pub area_pointer: Option<u8>,
	pub entrance: u8,
	pub entrance_mode: u8,
	pub entrance_control: u8,
}

#[derive(Debug,serde_derive::Deserialize,serde_derive::Serialize)]
enum Request {
	Flush,
	Frame(crate::Input),
	GetRam,
	GetState,
	Load(u32),
	LoadLevel(Level),
	LoadPath(std::path::PathBuf),
	Render(std::path::PathBuf),
	Reset,
	Restore,
	Save(u32),
	Snapshot,
}

struct EmulatorChild {
	parent: std::os::unix::net::UnixStream,

	emulator: retro_rs::Emulator,

	buf: Vec<u8>,
	save_path: std::path::PathBuf,

	save_buf: Vec<u8>,
}

impl EmulatorChild {
	fn save_path(&mut self, slot: u32) -> &std::path::Path {
		self.save_path.set_file_name(slot.to_string());
		&self.save_path
	}

	fn send_impl(&mut self, v: impl serde::Serialize + std::fmt::Debug)
	-> Result<(), failure::Error>
	{
		let base = self.buf.len();
		self.buf.extend(&0u32.to_le_bytes());
		let data = self.buf.len() as u32;
		bincode::serialize_into(&mut self.buf, &v)?;
		let end = self.buf.len() as u32;
		debug_assert!(end != data, "Zero sized message.");
		self.buf[base..][..4].copy_from_slice(&(end - data).to_le_bytes());

		self.parent.write_all(&self.buf)?;
		Ok(())
	}

	fn send_ok(&mut self, v: impl serde::Serialize + std::fmt::Debug)
	-> Result<(), failure::Error>
	{
		self.buf.clear();
		self.send_impl(v)
	}

	fn send_err(&mut self, e: failure::Error) -> Result<(), failure::Error> {
		self.buf.clear();
		self.buf.extend(&0u32.to_le_bytes());
		self.send_impl(format!("{} {}", e, e.backtrace()))
	}

	fn run(&mut self) -> ! {
		loop {
			if let Err(e) = self.run_one() {
				eprintln!("Child error: {:?}", e);
				self.send_err(e).unwrap();
			}
		}
	}

	fn run_one(&mut self) -> Result<(), failure::Error> {
		let mut size_buf = [0; 4];
		self.parent.read_exact(&mut size_buf[..])?;
		let size = u32::from_le_bytes(size_buf);

		self.buf.resize_with(size as usize, Default::default);
		self.parent.read_exact(&mut self.buf)?;

		let req = bincode::deserialize(&self.buf)?;
		Ok(match req {
			Request::Flush => self.send_ok(true)?,
			Request::Frame(input) => self.frame(input),
			Request::GetState => {
				let state = self.get_state();
				self.send_ok(state)?
			}
			Request::GetRam => {
				self.send_ok(self.get_ram().to_vec())?
			}
			Request::Load(slot) =>
				self.load(slot)
					.map_err(|e| e.context(format!("Loading {}", slot)))?,
			Request::LoadLevel(Level{world, level, area, area_pointer, entrance, entrance_mode, entrance_control}) => {
				let ram = self.emulator.system_ram_mut();

				ram[0x75F] = world - 1;
				ram[0x75C] = level - 1;
				ram[0x760] = area;

				ram[0x752] = entrance_mode;
				ram[0x751] = entrance;
				ram[0x710] = entrance_control;

				self.emulator.run([
					retro_rs::Buttons::new().start(true),
					retro_rs::Buttons::new(),
				]);

				if let Some(area_pointer) = area_pointer {
					let ram = self.emulator.system_ram_mut();
					ram[0x750] = area_pointer;
				}

				self.frame(crate::Input::default());
				while !matches!(self.get_state().mode, MajorMode::Game(GameMode::Core)) {
					self.frame(crate::Input::default());
				}
			}
			Request::LoadPath(path) =>
				self.load_path(&path)
					.map_err(|e| e.context(format!("Loading {:?}", path)))?,
			Request::Render(path) => self.render(&path)?,
			Request::Reset => {
				self.emulator.reset();
				for _ in 0..16 {
					self.frame(crate::Input::default());
				}
				while !matches!(self.get_state().mode, MajorMode::Title(TitleMode::Menu)) {
					self.frame(crate::Input::default());
				}
			}
			Request::Restore => self.restore()?,
			Request::Save(slot) =>
				self.save(slot)
					.map_err(|e| e.context(format!("Saving {}", slot)))?,
			Request::Snapshot => self.snapshot(),
		})
	}

	fn frame(&mut self, input: crate::Input) {
		// dbg!(&input);
		self.emulator.run([
			retro_rs::Buttons::new()
				.left(input.axis_h == crate::AxisInput::Negative)
				.right(input.axis_h == crate::AxisInput::Positive)
				.down(input.axis_v == crate::AxisInput::Negative)
				.up(input.axis_v == crate::AxisInput::Positive)
				.a(input.button_a)
				.b(input.button_b),
			retro_rs::Buttons::new(),
		]);
	}

	fn get_ram(&self) -> [u8; 0x800] {
		let mut ram = [0; 0x800];
		ram.copy_from_slice(&self.emulator.system_ram_ref()[..0x800]);
		ram
	}

	fn get_state(&self) -> RunResult {
		let mut ram = [0; 0x800];
		ram.copy_from_slice(&self.emulator.system_ram_ref()[..0x800]);

		let mode = match (ram[0x770], ram[0x772]) {
			(0, 0) => MajorMode::Title(TitleMode::Init),
			(0, 1) => MajorMode::Title(TitleMode::SetupGraphics),
			(0, 2) => MajorMode::Title(TitleMode::SetupGame),
			(0, 3) => MajorMode::Title(TitleMode::Menu),
			(1, 0) => MajorMode::Game(GameMode::LoadArea(Level {
				world: ram[0x7FD] + 1,
				level: ram[0x75C] + 1,
				area: ram[0x760],
				area_pointer: Some(ram[0x750]),
				entrance: ram[0x751],
				entrance_mode: ram[0x752],
				entrance_control: ram[0x710],
			})),
			(1, 1) => MajorMode::Game(GameMode::Graphics),
			(1, 2) => MajorMode::Game(GameMode::Setup),
			(1, 3) => MajorMode::Game(GameMode::Core),
			(2, 0) => MajorMode::Victory(VictoryMode::BridgeCollapse),
			(2, 1) => MajorMode::Victory(VictoryMode::Setup),
			(2, 2) => MajorMode::Victory(VictoryMode::Walk),
			(2, 3) => MajorMode::Victory(VictoryMode::PrintMessages),
			(2, 4) => MajorMode::Victory(VictoryMode::EndWorld),
			(id, _) => MajorMode::Other(id),
		};

		let r = RunResult {
			alive: ram[0x00E] != 11,
			level: (ram[0x75F] + 1, ram[0x75c] + 1),
			area: ram[0x760],
			mode,
			x: {
				let subpixel = ram[0x400];
				if matches!(mode, MajorMode::Game(GameMode::Core)) {
					debug_assert_eq!(
						subpixel & 0x0F, 0,
						"Unaligned subpixel 0x{:02x}", subpixel);
				}
				u32::from_be_bytes([0, ram[0x06D], ram[0x086], subpixel]) >> 4
			},
			speed: i16::from_be_bytes([ram[0x057], ram[0x705]]),
			change_area_timer: ram[0x6DE],
			hash: hash_state(ram),
		};

		r
	}

	pub fn restore(&mut self) -> Result<(), failure::Error> {
		match self.emulator.load(&self.save_buf) {
			true => Ok(()),
			false => Err(failure::format_err!("Load failed.")),
		}
	}

	pub fn load(&mut self, slot: u32) -> Result<(), failure::Error> {
		self.save_buf.clear();
		std::fs::File::open(self.save_path(slot))?
			.read_to_end(&mut self.save_buf)?;

		self.restore()
			.map_err(|e| e.context(format!("Restoring slot {}", slot)).into())
	}

	pub fn load_path(&mut self, path: &std::path::Path) -> Result<(), failure::Error> {
		std::fs::File::open(path)?
			.read_to_end(&mut self.save_buf)?;

		self.restore()
			.map_err(|e| e.context(format!("Restoring {:?}", path)).into())
	}

	pub fn snapshot(&mut self) {
		self.save_buf.resize_with(self.emulator.save_size(), Default::default);
		self.emulator.save(&mut self.save_buf);
	}

	pub fn save(&mut self, slot: u32) -> Result<(), failure::Error> {
		self.snapshot();

		self.save_path(slot);
		std::fs::write(&self.save_path, &self.save_buf)?;

		Ok(())
	}

	pub fn render(&self, path: &std::path::Path) -> Result<(), failure::Error> {
		let tmp = path.with_extension("tmp.png");
		let out = path.with_extension("png");
		retro_rs::FramebufferToImageBuffer::create_imagebuffer(&self.emulator)?
			.save(&tmp)?;
		std::fs::rename(tmp, out)?;

		Ok(())
	}
}

#[derive(Clone,Debug,serde::Deserialize,serde::Serialize)]
pub(crate) struct RunResult {
	pub mode: MajorMode,
	pub level: (u8, u8),

	pub alive: bool,
	pub area: u8,
	pub x: u32,
	pub speed: i16,
	pub change_area_timer: u8,
	pub hash: u64,
}

impl RunResult {
	#[cfg(test)]
	fn test() -> Self {
		RunResult {
			mode: MajorMode::Game(GameMode::Core),
			level: (u8::max_value(), u8::max_value()),

			alive: false,
			area: u8::max_value(),
			x: u32::max_value(),
			speed: i16::min_value(),
			change_area_timer: u8::max_value(),
			hash: u64::max_value(),
		}
	}

	fn max_future_speeds(&self) -> impl Iterator<Item=i8> {
		let mut speed = self.speed;

		let accel_high = 228;
		let accel_low = 152;
		let max_high = 40;
		let max_low = 24;

		(0..)
			.map(move |_| {
				let abs_speed = (speed.abs() >> 8) as u8;

				let waterlevel = false;
				let (accel, max) = if waterlevel {
					if speed >= 25 {
						(accel_high, max_high)
					} else {
						(accel_low, max_low)
					}
				} else if abs_speed >= 25 {
					(accel_high*2, max_high)
				} else if (speed + accel_low*2) >> 8 > max_low {
					// In this case a lower speed limit will hurt us more than the extra speed.
					(accel_high, max_high)
				} else {
					(accel_low*2, max_low)
				};

				speed += accel;
				speed -= (speed / 256 - max).max(0) << 8;
				(speed >> 8) as i8
			})
	}

	/// Minimum number of frames required to reach the given x subpixel.
	#[inline(never)]
	pub fn min_frames_to(&self, x: u32) -> u16 {
		let mut distance: i64 = x as i64 - self.x as i64;

		for (frame, speed) in self.max_future_speeds().enumerate() {
			if distance <= 0 {
				return frame as u16
			}

			distance -= speed as i64;
		}

		unreachable!();
	}
}

#[test]
fn test_accel_curve() {
	let mut res = RunResult::test();

	res.speed = 0;
	assert_eq!(
		res.max_future_speeds().take(32).collect::<Vec<_>>(),
		[
			1,
			2,
			3,
			4,
			5,
			7,
			8,
			9,
			10,
			11,
			13,
			14,
			15,
			16,
			17,
			19,
			20,
			21,
			22,
			23,
			24,
			25,
			27,
			29,
			31,
			32,
			34,
			36,
			38,
			40,
			40,
			40,
		]);

	res.speed = 0xFF;
	assert_eq!(
		res.max_future_speeds().take(32).collect::<Vec<_>>(),
		[
			2,
			3,
			4,
			5,
			6,
			8,
			9,
			10,
			11,
			12,
			14,
			15,
			16,
			17,
			18,
			19,
			21,
			22,
			23,
			24,
			25,
			27,
			29,
			30,
			32,
			34,
			36,
			38,
			39,
			40,
			40,
			40,
		]);

	res.speed = 40 << 8;
	assert_eq!(
		res.max_future_speeds().take(4).collect::<Vec<_>>(),
		[40, 40, 40, 40]);

	res.speed = 41 << 8;
	assert_eq!(
		res.max_future_speeds().take(4).collect::<Vec<_>>(),
		[40, 40, 40, 40]);
}

#[test]
fn test_min_frames_to() {
	let mut res = RunResult::test();
	res.x = 0;

	res.speed = -30;
	assert_eq!(res.min_frames_to(0), 0);

	res.speed = 0;
	assert_eq!(res.min_frames_to(1), 1);

	res.speed = 20 << 8;
	assert_eq!(res.min_frames_to(20), 1);

	res.speed = 0;
	assert_eq!(res.min_frames_to(100 << 8), 657);
}

pub fn hash_state(mut ram: [u8; 0x800]) -> u64 {
	for addr in WORK_RAM {
		ram[*addr as usize] = 0;
	}

	let mut hasher = twox_hash::XxHash64::default();
	std::hash::Hasher::write(&mut hasher, &ram);
	std::hash::Hasher::finish(&hasher)
}


#[derive(Copy,Clone,Debug,serde_derive::Deserialize,serde_derive::Serialize)]
pub(crate) enum MajorMode {
	Title(TitleMode),
	Game(GameMode),
	Victory(VictoryMode),
	Other(u8),
}

#[derive(Copy,Clone,Debug,serde_derive::Deserialize,serde_derive::Serialize)]
pub(crate) enum TitleMode {
	Init,
	SetupGraphics,
	SetupGame,
	Menu,
}

#[derive(Copy,Clone,Debug,serde_derive::Deserialize,serde_derive::Serialize)]
pub(crate) enum GameMode {
	LoadArea(Level),
	Graphics,
	Setup,
	Core,
}

#[derive(Copy,Clone,Debug,serde_derive::Deserialize,serde_derive::Serialize)]
pub(crate) enum VictoryMode {
	BridgeCollapse,
	Setup,
	Walk,
	PrintMessages,
	EndWorld,
}

impl MajorMode {
	// fn set(&self, ram: &mut [u8]) {
	// 	match self {
	// 		MajorMode::Title(minor) => {
	// 			ram[0x770] = 0;
	// 			ram[0x772] = *minor as u8;
	// 		}
	// 		MajorMode::Game(minor) => {
	// 			ram[0x770] = 1;
	// 			ram[0x772] = match minor {
	// 				GameMode::LoadArea{..} => 0,
	// 				GameMode::Graphics => 1,
	// 				GameMode::Setup => 2,
	// 				GameMode::Core => 3,
	// 			};
	// 		}
	// 		_ => panic!("Can't set mode {:?}", self),
	// 	}
	// }
}
