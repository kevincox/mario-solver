#[derive(Debug,serde::Deserialize,serde::Serialize)]
pub(crate) enum LogMsg {
	Explored{id: u64, timestamp: std::time::SystemTime},
	Found(LogState),
}

#[derive(Debug,serde::Deserialize,serde::Serialize)]
pub(crate) struct LogState {
	pub id: u64,
	pub parent: Option<u64>,

	pub input: crate::Input,

	pub frame: u16,
	pub save_state: Option<u32>,

	pub x: u32,
	pub speed: i16,
	pub score: u16,

	pub timestamp: std::time::SystemTime,
}

pub(crate) struct Logger {
	out: std::io::BufWriter<std::fs::File>,
}

impl Logger {
	pub fn new(path: &std::path::Path) -> Result<Self, failure::Error> {
		Ok(Logger {
			out: std::io::BufWriter::new(
				std::fs::OpenOptions::new()
					.create(true)
					.append(true)
					.open(path)?),
		})
	}

	pub fn log(&mut self, msg: &LogMsg) -> Result<(), failure::Error> {
		bincode::serialize_into(&mut self.out, msg)?;
		Ok(())
	}

	pub fn read_logs<'a>(dir: &'a std::path::Path)
		-> impl Iterator<Item=Result<LogMsg, failure::Error>> + 'a
	{
		let last_config =
			std::fs::read(dir.join("config.bin"))
				.map_err(|e| e.into())
				.and_then(|bytes|
					bincode::deserialize::<crate::bin_mario_solver::Config>(&bytes)
						.into());

		let (ok, err) = match last_config {
			Ok(ok) => (Some(ok), None),
			Err(err) => (None, Some(err)),
		};

		let ok = ok.into_iter().flat_map(move |config|
			(0..config.flag_threads)
				// .filter(|&t| t == 0)
				.map(move |thread| {
					let log = dir.join(format!("logs/{}", thread));
					let f = std::io::BufReader::new(std::fs::File::open(log)?);
					Ok(f)
				})
				.flat_map(|file| {
					let (ok, err) = match file {
						Ok(ok) => (Some(ok), None),
						Err(err) => (None, Some(err)),
					};
					let errors = err.into_iter().map(Err);
					let states = ok.into_iter().flat_map(|mut file|
						(0..)
							.map(move |_| -> Option<Result<LogMsg, failure::Error>> {
								match bincode::deserialize_from(&mut file) {
									Ok(state) => Some(Ok(state)),
									Err(e) => {
										if let bincode::ErrorKind::Io(e) = &*e {
											if e.kind() == std::io::ErrorKind::UnexpectedEof {
												return None
											}
										}
										Some(Err(e.into()))
									}
								}
							})
							.take_while(|o| o.is_some())
							.flatten());
					errors.chain(states)
				}));

		err.into_iter().map(|e| Err(e.into())).chain(ok)
	}
}
