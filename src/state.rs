#[derive(Clone,Debug,Eq,PartialEq,PartialOrd)]
pub(crate) struct SaveState {
	pub id: u32,
}

#[derive(Debug,Eq,PartialEq,PartialOrd)]
pub(crate) struct State {
	pub frame_count: u16,
	pub input: crate::Input,
	pub parent: u64,
	pub base: Option<SaveState>,
}
