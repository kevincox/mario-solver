struct Data<K, V> {
	astar: crate::AStar<K, V>,
	solution: Option<K>,
	working: u32,
}

pub(crate) struct AStarManager<K, V> {
	data: std::sync::Mutex<Data<K, V>>,
	condvar: std::sync::Condvar,
}

impl<
	K: Clone + std::fmt::Debug + Eq + std::hash::Hash,
	V: PartialOrd,
> AStarManager<K, V> {
	pub fn new(astar: crate::AStar<K, V>) -> Self {
		AStarManager {
			data: std::sync::Mutex::new(Data {
				astar,
				solution: None,
				working: 0,
			}),
			condvar: Default::default(),
		}
	}

	pub fn pop(&self) -> Option<AStarState<K, V>> {
		let mut data = self.data.lock().unwrap();
		loop {
			if data.solution.is_some() {
				return None
			}

			if let Some(state) = data.astar.pop() {
				data.working += 1;
				return Some(AStarState {
					state_key: state.0,
					score: state.1,
					data,
					manager: &self,
				})
			}

			if data.working == 0 {
				return None
			}

			data = self.condvar.wait(data).unwrap();
		}
	}

	pub fn report_solution(&self, solution: K) {
		self.data.lock().unwrap().solution = Some(solution);
	}

	pub fn solution(&self) -> Option<K> {
		self.data.lock().unwrap().solution.clone()
	}

	pub fn lock(&self) -> AStarLocked<K, V> {
		AStarLocked {
			data: self.data.lock().unwrap(),
		}
	}
}

impl<K, V> From<AStarManager<K, V>> for crate::AStar<K, V> {
	fn from(that: AStarManager<K, V>) -> Self {
		that.data.into_inner().unwrap().astar
	}
}

pub(crate) struct AStarLocked<'a, K, V> {
	data: std::sync::MutexGuard<'a, Data<K, V>>,
}

impl<'a, K, V> AStarLocked<'a, K, V> {
	pub fn astar(&mut self) -> &mut crate::AStar<K, V> {
		&mut self.data.astar
	}
}

pub(crate) struct AStarState<'a, K, V> {
	pub state_key: K,
	pub score: usize,
	data: std::sync::MutexGuard<'a, Data<K, V>>,
	manager: &'a AStarManager<K, V>,
}

impl<
	'a,
	K: Clone + std::fmt::Debug + Eq + std::hash::Hash,
	V: PartialOrd,
> AStarState<'a, K, V> {
	pub fn get(&self, k: &K) -> Option<&V> {
		self.data.astar.get(k)
	}

	pub fn release(self) -> AStarResultManager<'a, K, V> {
		AStarResultManager {
			state_key: self.state_key.clone(),
			score: self.score,
			manager: self.manager,
		}
	}
}

pub(crate) struct AStarResultManager<'a, K, V> {
	pub state_key: K,
	pub score: usize,
	manager: &'a AStarManager<K, V>,
}

impl<
	'a,
	K: Clone + std::fmt::Debug + std::fmt::UpperHex + Eq + std::hash::Hash,
	V: PartialOrd,
> AStarResultManager<'a, K, V> {
	pub fn add(&self, k: K, state: V, score: usize) -> bool {
		if score < self.score {
			eprintln!(
				"State {:016X} regressed score from {} to {}",
				k, self.score, score);
		}

		let is_new = self.manager.data.lock().unwrap().astar.add(k, state, score);
		self.manager.condvar.notify_one();
		is_new
	}
}

impl<'a, K, V> std::ops::Drop for AStarResultManager<'a, K, V> {
	fn drop(&mut self) {
		self.manager.data.lock().unwrap().working -= 1;
	}
}

