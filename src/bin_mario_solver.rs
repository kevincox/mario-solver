use std::io::Write;

const USAGE: &'static str = "
Usage: mario-solver [options]

Start Options:
	--world=<number>  Play the given world. [default: 1]
	--level=<number>  Play the given level. [default: 1]
	--area=<number>  Play the given area. [default: 0]
	--area-pointer=<number>  Load the given area pointer instead of the default one for the area.
	--entrance=<number>  Use the given entrance. [default: 0]
	--entrance-mode=<number>  Use the given entrance. [default: 0]

	--save=<path>  Start from the given save-state.

	--fast-start  Run right for a bit at the start of the level. This bypasses the slow acceleration solver. Only useful for development.

Solver Options:
	--core=<path>  The path to the libretro core. [default: /usr/lib/libretro/fceumm_libretro]
	--exit-x=<pixel>  The target x position to achieve. [default: 3162]
	--out=<path>  The directory to write state and output. [default: /tmp/mario-solver]
	--rom=<path>  The path to the Super Mario Bros ROM.

Misc Options:
	--debug  Generate periodic screenshots of current progress and print some logs.
	--threads=<count> -j  The directory to write state and output. [default: 2]

	--log  Log state for --resume.
	--resume  Continue the previous execution. Note that changing any Solver Options from the previous run may lead to unexpected results.

	--help  Show this message.
";

#[derive(PartialEq,serde_derive::Deserialize,serde_derive::Serialize)]
pub(crate) struct Config {
	flag_area: u8,
	flag_area_pointer: Option<u8>,
	flag_core: std::path::PathBuf,
	flag_debug: bool,
	flag_entrance: u8,
	flag_entrance_mode: u8,
	flag_exit_x: u16,
	flag_fast_start: bool,
	flag_level: u8,
	flag_log: bool,
	flag_out: std::path::PathBuf,
	flag_resume: bool,
	flag_rom: std::path::PathBuf,
	flag_save: Option<std::path::PathBuf>,
	pub flag_threads: u8,
	flag_world: u8,
	flag_x: Option<u32>,
}

struct Solver {
	astar: crate::AStarManager<u64, crate::State>,
	config: Config,
	exit_x: u32,
	save_count: std::sync::atomic::AtomicU32,
}

impl Solver {
	fn make_emulator(&self) -> Result<crate::Emulator, failure::Error> {
		crate::Emulator::new(
			&self.config.flag_core,
			&self.config.flag_rom,
			&self.config.flag_out)
	}

	fn make_first_save(&mut self) -> Result<(), failure::Error> {
		let mut emu = self.make_emulator()?;

		if let Some(save) = &self.config.flag_save {
			assert!(self.config.flag_area == 0, "Can not set --area when using --save");
			assert!(self.config.flag_area_pointer.is_none(), "Can not set --area-pointer when using --save");
			assert!(self.config.flag_entrance == 0, "Can not set --entrance when using --save");
			assert!(self.config.flag_entrance_mode == 0, "Can not set --entrance-mode when using --save");
			assert!(!self.config.flag_fast_start, "Can not set --fast-start when using --save");
			assert!(self.config.flag_level == 1, "Can not set --level when using --save");
			assert!(self.config.flag_world == 1, "Can not set --world when using --save");
			emu.load_path(save.clone())?;
		} else {
			emu.load_level(crate::Level {
				world: self.config.flag_world,
				level: self.config.flag_level,
				area: self.config.flag_area,
				area_pointer: self.config.flag_area_pointer,
				entrance: self.config.flag_entrance,
				entrance_mode: self.config.flag_entrance_mode,
				entrance_control: 2,
			})?;
			if self.config.flag_fast_start {
				emu.run_right()?;
			}
		}

		// emu.render(self.config.flag_out.join("debug/0.png"));
		// return Ok(());

		emu.save(0)?;
		emu.flush_full()?;

		Ok(())
	}
}

struct SolverThread<'a> {
	emu: crate::Emulator,
	found_new: u64,
	logger: Option<crate::Logger>,
	screenshot_throttle: throttle::Throttle,
	script: crate::Script,
	solver: &'a Solver,
	started: std::time::SystemTime,
	thread_id: u8,
	visited: u64,
}

impl<'a> SolverThread<'a> {
	fn solve(&mut self) {
		while let Some(state) = self.solver.astar.pop() {
			self.script.inputs.clear();
			let mut current_state = state.state_key;
			loop {
				let s = state.get(&current_state).unwrap();
				if let Some(id) = &s.base {
					self.script.base = id.clone();
					self.script.inputs.reverse();
					break
				}
				self.script.inputs.push(s.input);
				current_state = s.parent;
			}

			if self.script.inputs.is_empty() && state.state_key != 0 {
				// This state has already been handled.
				continue
			}

			let frame_count = state.get(&state.state_key).unwrap().frame_count + 1;

			let state = state.release();

			for attempt in 1..=3 {
				let r = self.process_state(&state, frame_count);
				if let Ok(()) = r {
					break
				} else {
					eprintln!("Attempt {} at processing {:?} failed.",
						attempt, state.state_key);
					self.emu = self.solver.make_emulator().unwrap();
				}
			}
		}

		eprintln!("Thread {} done.", self.thread_id);
	}

	fn process_state(&mut self,
		state: &crate::AStarResultManager<u64, crate::State>,
		frame_count: u16,
	) -> Result<(), failure::Error> {
		self.emu.run(&self.script)?;
		let slot = if self.script.inputs.len() >= 60 {
			let id = self.solver.save_count.fetch_add(1,
				std::sync::atomic::Ordering::Relaxed);
			self.emu.save(id)?;
			self.emu.flush_full()?;
			let state_key = state.state_key;
			let mut astar = self.solver.astar.lock();
			let astar = astar.astar();
			let state_info = astar.get_mut(&state_key).unwrap();
			state_info.base = Some(crate::SaveState { id });
			Some(id)
		} else {
			self.emu.snapshot()?;
			None
		};

		let parent = state.state_key;

		for input in crate::POSSIBLE_INPUTS {
			let input = *input;

			if input != crate::POSSIBLE_INPUTS[0] {
				self.emu.restore()?;
			}
			let res = self.emu.run_one(input)?;

			let mut score = frame_count;
			if res.change_area_timer > 0 {
				score += res.change_area_timer as u16;
			} else {
				score += res.min_frames_to(self.solver.exit_x);
			}

			self.visited += 1;

			if let Some(logger) = &mut self.logger {
				logger.log(&crate::LogMsg::Found(crate::LogState {
					id: res.hash,
					parent: Some(parent),
					input,

					save_state: slot,
					frame: frame_count,

					x: res.x,
					speed: res.speed,
					score,

					timestamp: std::time::SystemTime::now(),
				}))?;
			}

			let new = state.add(res.hash, crate::State {
				parent,
				input,
				frame_count,
				base: None,
			}, score as usize);
			if !new {
				continue
			}

			self.found_new += 1;

			if self.solver.config.flag_debug
				&& self.screenshot_throttle.accept().is_ok()
			{
				self.emu.render(
					self.solver.config.flag_out.join(format!("debug/{}.png", self.thread_id)));
				eprintln!(
					"{:6} {:016X} {} -> {:016X} x{:5} dx{:5} s{:5} {}/s {}/s new",
					frame_count,
					parent,
					input,
					res.hash,
					res.x,
					res.speed,
					score,
					self.visited.checked_div(self.started.elapsed()?.as_secs()).unwrap_or(0),
					self.found_new.checked_div(self.started.elapsed()?.as_secs()).unwrap_or(0));
			}

			match res.mode {
				crate::MajorMode::Game(crate::GameMode::LoadArea{..}) |
				crate::MajorMode::Victory(crate::VictoryMode::PrintMessages) => {
					eprintln!("Exit: {:?}", res);
					self.solver.astar.report_solution(res.hash);
					break;
				}
				_ => {}
			}
		}

		if let Some(logger) = &mut self.logger {
			logger.log(&crate::LogMsg::Explored {
				id: state.state_key,
				timestamp: std::time::SystemTime::now(),
			})?;
		}

		Ok(())
	}
}

pub fn main() -> Result<(), failure::Error> {
	let config: Config = docopt::Docopt::new(USAGE)
		.and_then(|d| d.deserialize())
		.unwrap_or_else(|e| e.exit());

	if config.flag_log {
		std::fs::create_dir_all(config.flag_out.join("logs")).unwrap();
	}

	std::fs::create_dir_all(config.flag_out.join("saves")).unwrap();
	std::fs::create_dir_all(config.flag_out.join("solution")).unwrap();
	if config.flag_debug {
		std::fs::create_dir_all(config.flag_out.join("debug")).unwrap();
	}

	let mut astar = crate::AStar::new(0, crate::State {
		parent: 0,
		frame_count: 0,
		input: crate::POSSIBLE_INPUTS[0],
		base: Some(crate::SaveState { id: 0 }),
	}, 0);

	if config.flag_resume {
		let mut loaded = 0;
		let mut explored = std::collections::HashSet::new();
		for msg in crate::Logger::read_logs(&config.flag_out) {
			match msg? {
				crate::LogMsg::Explored{id, timestamp: _} => {
					explored.insert(id);
				}
				crate::LogMsg::Found(state) => {
					loaded += 1;
					astar.add(state.id, crate::State {
						parent: state.parent.unwrap_or(0),
						frame_count: state.frame,
						base: state.save_state.map(|id| crate::SaveState { id }),
						input: state.input,
					}, state.score.into());
				}
			}
		}
		astar.dequeue_all(explored);
		eprintln!("Loaded {} items from previous run.", loaded);
	} else if config.flag_log {
		for thread in 0..config.flag_threads {
			let _ = std::fs::remove_file(config.flag_out.join(format!("logs/{}", thread)));
		}

		let mut logger = crate::Logger::new(
			&config.flag_out.join("logs/0")).unwrap();
		logger.log(&crate::LogMsg::Found(crate::LogState {
			id: 0,
			parent: None,
			input: crate::Input::default(),

			save_state: Some(0),
			frame: 0,

			x: 0,
			speed: 0,
			score: 0,

			timestamp: std::time::SystemTime::now(),
		})).unwrap();
	}

	std::fs::write(config.flag_out.join("config.bin"), bincode::serialize(&config)?)?;

	let mut solver = Solver {
		astar: crate::AStarManager::new(astar),
		exit_x: config.flag_exit_x as u32 * 16,
		save_count: std::sync::atomic::AtomicU32::new(1),

		config,
	};

	solver.make_first_save()?;

	crossbeam::scope(|s| {
		let solver = &solver;

		for thread_id in 0..solver.config.flag_threads {
			s.spawn(move |_| {
				SolverThread {
					emu: solver.make_emulator().unwrap(),
					found_new: 0,
					logger: if solver.config.flag_log {
						Some(crate::Logger::new(
							&solver.config.flag_out.join(format!("logs/{}", thread_id))).unwrap())
					} else {
						None
					},
					screenshot_throttle: throttle::Throttle::new(
						std::time::Duration::from_secs(1),
						1),
					script: crate::Script {
						base: crate::SaveState { id: 0 },
						inputs: Vec::new(),
					},
					solver: &solver,
					started: std::time::SystemTime::now(),
					thread_id,
					visited: 0,
				}.solve();
			});
		}
	}).unwrap();

	let mut emu = solver.make_emulator()?;
	let Solver{astar, config, ..} = solver;

	let solution = match astar.solution() {
		Some(solution) => solution,
		None => {
			eprintln!("No solution.");
			std::process::exit(1);
		}
	};
	eprintln!("Solution found: {}", solution);
	let astar: crate::AStar<_, _> = astar.into();

	let mut script = crate::Script {
		base: crate::SaveState { id: 0 },
		inputs: Vec::new(),
	};
	fn final_script(script: &mut crate::Script, astar: &crate::AStar<u64, crate::State>, state: u64) {
		if state == 0 {
			return
		}
		let state = astar.get(&state).unwrap();
		final_script(script, astar, state.parent);
		script.inputs.push(state.input);
	}
	final_script(&mut script, &astar, solution);

	eprintln!("Solution:");

	let mut inputs = std::fs::File::create(config.flag_out.join("solution/inputs.txt")).unwrap();
	emu.load(&script.base).unwrap();
	for (frame, input) in script.inputs.into_iter().enumerate() {
		let input_log = format!("{} {}\n", frame, input);
		print!("{}", input_log);
		inputs.write_all(input_log.as_bytes()).unwrap();
		emu.run_one(input).unwrap();
		emu.render(config.flag_out.join(format!("solution/{}.png", frame)));
	}
	eprintln!("Solution rendered.");

	Ok(())
}
