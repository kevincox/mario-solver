struct Queue<T> {
	queue: Vec<Vec<T>>,
	min: usize,
}

impl<T: Eq + std::fmt::Debug + std::hash::Hash> Queue<T> {
	fn new(first: T, score: usize) -> Self {
		let mut queue = Vec::new();
		queue.resize_with(score + 10, Default::default);
		queue[score] = vec![first];

		Queue {
			queue,
			min: score,
		}
	}

	fn add(&mut self, e: T, score: usize) {
		if score < self.min {
			self.min = score;
		}

		let slot = score;

		if self.queue.len() <= slot {
			if self.queue.capacity() <= slot {
				self.queue.reserve(slot);
			}
			self.queue.resize_with(self.queue.capacity(), Default::default);
		}

		self.queue[slot].push(e);
	}

	fn pop(&mut self) -> Option<(T, usize)> {
		while self.min < self.queue.len() {
			if let Some(e) = self.queue[self.min].pop() {
				return Some((e, self.min))
			}
			self.queue[self.min].shrink_to_fit();

			self.min += 1;
		}

		None
	}

	fn dequeue_all(&mut self, ids: std::collections::HashSet<T>) {
		for bucket in &mut self.queue {
			bucket.retain(|id| !ids.contains(id));
		}
	}
}

pub(crate) struct AStar<K, V> {
	queue: Queue<K>,
	known: std::collections::HashMap<K, V>,
}

impl<K: Clone + std::fmt::Debug + Eq + std::hash::Hash, V: PartialOrd> AStar<K, V> {
	pub fn new(k: K, v: V, score: usize) -> Self {
		let mut known = std::collections::HashMap::new();
		known.insert(k.clone(), v);

		AStar {
			queue: Queue::new(k, score),
			known,
		}
	}

	pub fn pop(&mut self) -> Option<(K, usize)> {
		self.queue.pop()
	}

	pub fn get(&self, k: &K) -> Option<&V> {
		self.known.get(k)
	}

	pub fn get_mut(&mut self, k: &K) -> Option<&mut V> {
		self.known.get_mut(k)
	}

	/// Return true if the state is previously unknown.
	pub fn add(&mut self, k: K, v: V, score: usize) -> bool {
		match self.known.entry(k.clone()) {
			std::collections::hash_map::Entry::Occupied(mut e) => {
				if &v >= e.get() {
					return false
				}
				*e.get_mut() = v
			}
			std::collections::hash_map::Entry::Vacant(e) => {
				e.insert(v);
			}
		}
		self.queue.add(k, score);
		true
	}

	pub fn dequeue_all(&mut self, ids: std::collections::HashSet<K>) {
		self.queue.dequeue_all(ids);
	}
}
