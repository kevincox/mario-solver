use std::fmt::Write as _;

const USAGE: &'static str = "
Usage: mario-solver-dump-ram [options]

Start Options:
	--save=<path>  Start from the given save-state.

Solver Options:
	--core=<path>  The path to the libretro core. [default: /usr/lib/libretro/fceumm_libretro]
	--rom=<path>  The path to the Super Mario Bros ROM.

	--help  Show this message.
";

#[derive(PartialEq,serde_derive::Deserialize,serde_derive::Serialize)]
pub(crate) struct Config {
	flag_core: std::path::PathBuf,
	flag_rom: std::path::PathBuf,
	flag_save: std::path::PathBuf,
}

pub fn main() -> Result<(), failure::Error> {
	let config: Config = docopt::Docopt::new(USAGE)
		.and_then(|d| d.deserialize())
		.unwrap_or_else(|e| e.exit());

	let mut emu = crate::Emulator::new(
		&config.flag_core,
		&config.flag_rom,
		std::path::Path::new("/dev/null"))?;

	emu.load_path(config.flag_save)?;
	let base_ram = emu.get_ram()?;

	let rams = crate::POSSIBLE_INPUTS.iter()
		.map(|input| {
			emu.restore()?;
			let res = emu.run_one(*input)?;
			dbg!(res);
			emu.get_ram()
		})
		.collect::<Result<Vec<_>,_>>()?;
	
	let mut buf = String::new();
	for (addr, base_byte) in base_ram.iter().enumerate() {
		if crate::WORK_RAM.contains(&(addr as u16)) { continue }

		buf.clear();

		let mut values = [0u8; 256];
		for (i, _) in crate::POSSIBLE_INPUTS.iter().enumerate() {
			values[rams[i][addr] as usize] += 1;
		}

		values.iter()
			.enumerate()
			.map(|(value, count)| (value as u8, *count))
			.filter(|(_, count)| *count > 0)
			.filter(|(_, count)| *count < crate::POSSIBLE_INPUTS.len() as u8)
			.for_each(|(value, _)| {
				write!(buf, "\n\t0x{:02x} ", value).unwrap();
				for (i, input) in crate::POSSIBLE_INPUTS.iter().enumerate() {
					if rams[i][addr] != value { continue }
					write!(buf, "{}", input).unwrap();
				}
			});

		if buf.is_empty() { continue }
		println!("0x{:03x}: was 0x{:02x}{}", addr, base_byte, buf);
	}

	Ok(())
}
