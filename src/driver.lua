local vstruct = require "vstruct"
vstruct.cache = true

-- emu.speedmode "nothrottle"

function read_int()
	return tonumber(connection:receive("*l"))
end

function run_input(id, save)
	-- emu.message("Input-"..id)
	joypad.set(1, POSSIBLE_INPUTS[id+1])
	emu.frameadvance()

	-- emu.print("Disance: "..memory.readbyteunsigned(0x000E))
	-- emu.message("FC1:"
	-- 	.." "..memory.readbyteunsigned(0x10)
	-- 	.." "..memory.readbyteunsigned(0x15)
	-- 	.." "..memory.readbyteunsigned(0xA2)
	-- 	.." "..memory.readbyteunsigned(0xFD)
	-- )

	if memory.readbyteunsigned(0x000E) == 11 then
		return false
	end

	local saved
	if save then
		saved = 1
	else
		saved = 2
		save_slot = savestate.object(2)
		-- savestate.persist(save_slot)
		savestate.save(save_slot)
	end
	ram = memory.readbyterange(0, 0x0800)
	fc = emu.framecount()

	connection:send(vstruct.write("u1u1u2c2", {saved, id, fc, ram}))

	return true
end

while true do
	connection:send("\0") -- Ready

	command = read_int()
	if command ~= 1 then
		emu.message("Unknown command "..command)
		break
	end

	save_slot = savestate.object(10)
	savestate.persist(save_slot)
	savestate.load(save_slot)
	local alive = true

	emu.message("Frame: "..emu.framecount())

	inputs = read_int()
	for i=1,inputs do
		input = read_int()
		if alive then
			alive = run_input(input)
		end
	end

	for i=1,59 do
		if not alive then
			break
		end
		alive = run_input(0)
	end

	if alive then
		run_input(0, true)
	end
end
