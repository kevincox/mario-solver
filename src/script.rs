#[derive(Clone,Copy,Debug,PartialEq,Eq,PartialOrd,Ord,serde_derive::Deserialize,serde_derive::Serialize)]
pub(crate) struct Input {
	pub axis_h: AxisInput,
	pub axis_v: AxisInput,
	pub button_a: bool,
	pub button_b: bool,
}

impl Input {
	#[allow(unused)]
	pub fn left(&self) -> Self {
		Input{axis_h: AxisInput::Negative, ..*self}
	}

	#[allow(unused)]
	pub fn right(&self) -> Self {
		Input{axis_h: AxisInput::Positive, ..*self}
	}

	#[allow(unused)]
	pub fn down(&self) -> Self {
		Input{axis_v: AxisInput::Negative, ..*self}
	}

	#[allow(unused)]
	pub fn a(&self, pressed: bool) -> Self {
		Input{button_a: pressed, ..*self}
	}

	#[allow(unused)]
	pub fn b(&self, pressed: bool) -> Self {
		Input{button_b: pressed, ..*self}
	}
}

impl Default for Input {
	fn default() -> Self {
		Input {
			axis_h: AxisInput::Neutral,
			axis_v: AxisInput::Neutral,
			button_a: false,
			button_b: false,
		}
	}
}

impl std::fmt::Display for Input {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		f.write_str("[")?;
		f.write_str(match self.axis_h {
			AxisInput::Positive => "R",
			AxisInput::Neutral => " ",
			AxisInput::Negative => "L",
		})?;
		f.write_str(match self.axis_v {
			AxisInput::Positive => "U",
			AxisInput::Neutral => " ",
			AxisInput::Negative => "D",
		})?;
		f.write_str(match self.button_a {
			true => "A",
			false => " ",
		})?;
		f.write_str(match self.button_b {
			true => "B",
			false => " ",
		})?;
		f.write_str("]")
	}
}

#[derive(Clone,Copy,Debug,PartialEq,Eq,PartialOrd,Ord,serde_derive::Deserialize,serde_derive::Serialize)]
pub(crate) enum AxisInput {
	Negative,
	Neutral,
	Positive,
}

#[derive(Debug)]
pub(crate) struct Script {
	pub base: crate::SaveState,
	pub inputs: Vec<Input>,
}

use AxisInput as A;
pub(crate) const POSSIBLE_INPUTS: &[Input] = &[
	Input{axis_h: A::Neutral, axis_v: A::Neutral, button_a: false, button_b: false},
	Input{axis_h: A::Neutral, axis_v: A::Neutral, button_a: false, button_b: true},
	Input{axis_h: A::Neutral, axis_v: A::Neutral, button_a: true, button_b: false},
	Input{axis_h: A::Neutral, axis_v: A::Neutral, button_a: true, button_b: true},
	Input{axis_h: A::Neutral, axis_v: A::Positive, button_a: false, button_b: false},
	Input{axis_h: A::Neutral, axis_v: A::Positive, button_a: false, button_b: true},
	Input{axis_h: A::Neutral, axis_v: A::Positive, button_a: true, button_b: false},
	Input{axis_h: A::Neutral, axis_v: A::Positive, button_a: true, button_b: true},
	Input{axis_h: A::Neutral, axis_v: A::Negative, button_a: false, button_b: false},
	Input{axis_h: A::Neutral, axis_v: A::Negative, button_a: false, button_b: true},
	Input{axis_h: A::Neutral, axis_v: A::Negative, button_a: true, button_b: false},
	Input{axis_h: A::Neutral, axis_v: A::Negative, button_a: true, button_b: true},
	Input{axis_h: A::Positive, axis_v: A::Neutral, button_a: false, button_b: false},

	Input{axis_h: A::Positive, axis_v: A::Neutral, button_a: true, button_b: false},
	Input{axis_h: A::Positive, axis_v: A::Neutral, button_a: true, button_b: true},
	Input{axis_h: A::Positive, axis_v: A::Positive, button_a: false, button_b: false},
	Input{axis_h: A::Positive, axis_v: A::Positive, button_a: false, button_b: true},
	Input{axis_h: A::Positive, axis_v: A::Positive, button_a: true, button_b: false},
	Input{axis_h: A::Positive, axis_v: A::Positive, button_a: true, button_b: true},
	Input{axis_h: A::Positive, axis_v: A::Negative, button_a: false, button_b: false},
	Input{axis_h: A::Positive, axis_v: A::Negative, button_a: false, button_b: true},
	Input{axis_h: A::Positive, axis_v: A::Negative, button_a: true, button_b: false},
	Input{axis_h: A::Positive, axis_v: A::Negative, button_a: true, button_b: true},
	Input{axis_h: A::Negative, axis_v: A::Neutral, button_a: false, button_b: false},
	Input{axis_h: A::Negative, axis_v: A::Neutral, button_a: false, button_b: true},
	Input{axis_h: A::Negative, axis_v: A::Neutral, button_a: true, button_b: false},
	Input{axis_h: A::Negative, axis_v: A::Neutral, button_a: true, button_b: true},
	Input{axis_h: A::Negative, axis_v: A::Positive, button_a: false, button_b: false},
	Input{axis_h: A::Negative, axis_v: A::Positive, button_a: false, button_b: true},
	Input{axis_h: A::Negative, axis_v: A::Positive, button_a: true, button_b: false},
	Input{axis_h: A::Negative, axis_v: A::Positive, button_a: true, button_b: true},
	Input{axis_h: A::Negative, axis_v: A::Negative, button_a: false, button_b: false},
	Input{axis_h: A::Negative, axis_v: A::Negative, button_a: false, button_b: true},
	Input{axis_h: A::Negative, axis_v: A::Negative, button_a: true, button_b: false},
	Input{axis_h: A::Negative, axis_v: A::Negative, button_a: true, button_b: true},

	Input{axis_h: A::Positive, axis_v: A::Neutral, button_a: false, button_b: true},
];
