const USAGE: &'static str = "
Usage: mario-solver-sqlite [options] <database>

Loads the logs of a mario-solver run into a sqlite database for analysis.

Solver Options:
	--out=<path>  The mario-solver out directory. [default: /tmp/mario-solver]

Misc Options:
	--help  Show this message.
";

#[derive(PartialEq,serde_derive::Deserialize,serde_derive::Serialize)]
struct Args {
	arg_database: std::path::PathBuf,
	flag_out: std::path::PathBuf,
}

fn make_connection(path: &std::path::Path)
	-> Result<rusqlite::Connection, failure::Error>
{
	let connection = rusqlite::Connection::open(path)?;
	connection.execute_batch(r###"
		PRAGMA synchronous = OFF;
		PRAGMA journal_mode = OFF;
		PRAGMA foreign_keys = OFF;
	"###)?;
	Ok(connection)
}

pub fn main() -> Result<(), failure::Error> {
	let args: Args = docopt::Docopt::new(USAGE)
		.and_then(|d| d.deserialize())
		.unwrap_or_else(|e| e.exit());

	match std::fs::remove_file(&args.arg_database) {
		Ok(()) => {},
		Err(e) => {
			if e.kind() != std::io::ErrorKind::NotFound {
				return Err(e.into())
			}
		}
	}

	let connection = make_connection(&args.arg_database)?;
	connection.execute_batch(r###"
		CREATE TABLE state (
			id BIGINT NOT NULL PRIMARY KEY,

			save BIGINT,

			x INT NOT NULL,
			speed INT NOT NULL
		);

		CREATE TABLE path (
			parent BIGINT NOT NULL REFERENCES state(id),
			child BIGINT NOT NULL REFERENCES state(id),

			input TEXT NOT NULL,

			frame INT NOT NULL,
			score INT NOT NULL,

			-- Seconds since epoch.
			found REAL NOT NULL,

			PRIMARY KEY (parent, child, input)
		);

	"###)?;

	let mut insert_state = connection.prepare(r###"
		INSERT INTO state VALUES (?, ?, ?, ?)
			ON CONFLICT DO NOTHING;
	"###)?;
	let mut insert_path = connection.prepare(r###"
		INSERT INTO path VALUES (?, ?, ?, ?, ?, ?)
			ON CONFLICT DO NOTHING;
	"###)?;

	for msg in crate::Logger::read_logs(&args.flag_out) {
		match msg? {
			crate::LogMsg::Found(state) => {
				insert_state.execute(&[
					&(state.id as i64) as &dyn rusqlite::ToSql,
					&state.save_state,
					&state.x,
					&state.speed,
				])?;
				if let Some(parent) = state.parent {
					insert_path.execute(&[
						&(parent as i64),
						&(state.id as i64) as &dyn rusqlite::ToSql,
						&state.input.to_string(),
						&state.frame,
						&state.score,
						&state.timestamp
							.duration_since(std::time::SystemTime::UNIX_EPOCH)?
							.as_secs_f64(),
					])?;
				}
			}
			crate::LogMsg::Explored{..} => {}
		}
	}

	eprintln!("Values inserted.");

	connection.execute_batch(r###"
		CREATE INDEX path_child_parent ON path(child, parent);
	"###)?;

	eprintln!("Indexes created;.");

	connection.execute_batch("ANALYZE")?;
	eprintln!("ANALYZE completed.");

	connection.execute_batch("VACUUM")?;
	eprintln!("VACUUM completed.");

	connection.execute_batch("PRAGMA foreign_key_check")?;
	eprintln!("PRAGMA foreign_key_check completed.");

	Ok(())
}
